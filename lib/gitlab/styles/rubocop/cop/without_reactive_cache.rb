# frozen_string_literal: true

module Gitlab
  module Styles
    module Rubocop
      module Cop
        # Cop that prevents the use of `without_reactive_cache`
        class WithoutReactiveCache < RuboCop::Cop::Cop
          MSG = 'without_reactive_cache is for debugging purposes only. Please use with_reactive_cache.'

          def on_send(node)
            return unless node.children[1] == :without_reactive_cache

            add_offense(node, location: :selector)
          end
        end
      end
    end
  end
end

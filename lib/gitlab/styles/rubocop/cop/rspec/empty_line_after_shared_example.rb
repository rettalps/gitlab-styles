# frozen_string_literal: true

require 'rubocop-rspec'
require_relative 'base'

module Gitlab
  module Styles
    module Rubocop
      module Cop
        module RSpec
          # Checks if there is an empty line after shared example blocks.
          #
          # @example
          #   # bad
          #   RSpec.describe Foo do
          #     it_behaves_like 'do this first'
          #     it_behaves_like 'does this' do
          #     end
          #     it_behaves_like 'does that' do
          #     end
          #     it_behaves_like 'do some more'
          #   end
          #
          #   # good
          #   RSpec.describe Foo do
          #     it_behaves_like 'do this first'
          #     it_behaves_like 'does this' do
          #     end
          #
          #     it_behaves_like 'does that' do
          #     end
          #
          #     it_behaves_like 'do some more'
          #   end
          #
          #   # fair - it's ok to have non-separated without blocks
          #   RSpec.describe Foo do
          #     it_behaves_like 'do this first'
          #     it_behaves_like 'does this'
          #   end
          #
          class EmptyLineAfterSharedExample < Base
            extend RuboCop::Cop::AutoCorrector
            include RuboCop::RSpec::EmptyLineSeparation

            MSG = 'Add an empty line after `%<example>s` block.'

            def_node_matcher :shared_examples,
                             (SharedGroups::ALL + Includes::ALL).block_pattern

            def on_block(node)
              shared_examples(node) do
                break if last_child?(node)

                missing_separating_line_offense(node) do |method|
                  format(MSG, example: method)
                end
              end
            end
          end
        end
      end
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/redirect_with_status'

RSpec.describe Gitlab::Styles::Rubocop::Cop::RedirectWithStatus do
  subject(:cop) { described_class.new }

  let(:controller_fixture_without_status) do
    %q(
      class UserController < ApplicationController
        def show
          user = User.find(params[:id])
          redirect_to user_path if user.name == 'John Wick'
        end

        def destroy
          user = User.find(params[:id])

          if user.destroy
            redirect_to root_path
          else
            render :show
          end
        end
      end
    )
  end

  let(:controller_fixture_with_status) do
    %q(
      class UserController < ApplicationController
        def show
          user = User.find(params[:id])
          redirect_to user_path if user.name == 'John Wick'
        end

        def destroy
          user = User.find(params[:id])

          if user.destroy
            redirect_to root_path, status: 302
          else
            render :show
          end
        end
      end
    )
  end

  context 'when in controller' do
    before do
      allow(cop).to receive(:in_controller?).and_return(true)
    end

    it 'registers an offense when a "destroy" action uses "redirect_to" without "status"' do
      expect_offense(<<-'RUBY')
        def destroy
          user = User.find(params[:id])

          if user.destroy
            redirect_to root_path
            ^^^^^^^^^^^ Do not use "redirect_to" without "status" in "destroy" action
          else
            render :show
          end
        end
      RUBY
    end

    it 'does not register an offense when a "destroy" action uses "redirect_to" with "status"' do
      expect_no_offenses(<<~RUBY)
        def destroy
          user = User.find(params[:id])

          if user.destroy
            redirect_to root_path, status: 302
          else
            render :show
          end
        end
      RUBY
    end
  end

  context 'when outside of controller' do
    it 'registers no offense' do
      expect_no_offenses(controller_fixture_without_status)
      expect_no_offenses(controller_fixture_with_status)
    end
  end
end
